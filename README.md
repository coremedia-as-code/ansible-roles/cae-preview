# CoreMedia - cae-preview


## Ports

```
40905
40999
40998
40980
40909
```

## dependent services

### replication-live-server
```
cae_preview:
  repository:
    url: http://content-management-server.int:40180/content-management-server/ior
```

### solr
```
cae_preview:
  solr:
    url: http://127.0.0.1:40080/solr
```
### elastic
```
cae_preview:
  elastic:
    solr:
      url: http://127.0.0.1:40080/solr
```
