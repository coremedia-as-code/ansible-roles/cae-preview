import pytest
import os
import yaml
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture()
def AnsibleDefaults():
    with open("../../defaults/main.yml", 'r') as stream:
        return yaml.load(stream)


@pytest.mark.parametrize("dirs", [
    "/tmp/deployment_artefacts",
    "/etc/ansible/facts.d",
    "/opt/coremedia/cae-preview-1/server-lib",
    "/opt/coremedia/cae-preview-1/common-lib",
    "/opt/coremedia/cae-preview-1/current/webapps/cae-preview-1/WEB-INF/properties/corem",
    "/var/cache/coremedia/cae-preview-1",
    "/var/cache/coremedia/cae-preview-1/persistent-transformed-blobcache"
])
def test_directories(host, dirs):
    d = host.file(dirs)
    assert d.is_directory
    assert d.exists


@pytest.mark.parametrize("files", [
    "/etc/ansible/facts.d/cae-preview-1.fact",
    "/opt/coremedia/cae-preview-1/common-lib/coremedia-tomcat.jar",
    "/opt/coremedia/cae-preview-1/current/bin/setenv.sh",
    "/opt/coremedia/cae-preview-1/cae-preview-1.properties"
])
def test_files(host, files):
    f = host.file(files)
    assert f.exists
    assert f.is_file


def test_user(host):
    assert host.user("coremedia").exists
    assert host.user("cae-preview-1").exists
    assert host.group("coremedia").exists
